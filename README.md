# Python: Countdown App


#### Project Outline

In this project we will take a spreadsheet and write functionality which will do some basic data manipulation

![Image 1](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image1.png?ref=main)

In the functionality we will achieve the following
-	List each company with respective product count
-	List each company with respective total inventory value
-	List products with inventory less than 10
-	Calculate and write inventory value for each product into a spreadsheet, thus creating a new column

#### Lets get started

We will start by moving out excel file in the same directory with the main.py

Now that we want to work with the spreadsheet, we can install a python package to work with it

We can use the package openpyxl

![Image 2](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image2.png?ref=main)

And then install the package using the below command


```
pip install openpyxl
```

And can see it successfully installed

![Image 3](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image3.png?ref=main)

![Image 4](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image4.png?ref=main)

Can then import it to get the functionalities

![Image 5](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image5.png?ref=main)

Now to load and read the spreadsheet

![Image 6](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image6.png?ref=main)

#### List each company with respective product count

Lets now save it as a variable, reference the sheet name and save that as a variable as ‘product_list’


![Image 7](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image7.png?ref=main)

Lets get to our first task, which is to list each company with respective product count or how many products we have per supplier

Lets define products per supplier and set it as a dictionary

![Image 8](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image8.png?ref=main)

Since we are going through a list, we can implement a for loop for the number of items in the spreadsheet

![Image 9](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image9.png?ref=main)

Can even test it, which will tell us how many times the code will iterate

![Image 10](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image10.png?ref=main)

Can then implement the range so that the loop can go through each row

And since the data starts from row 2

![Image 11](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image11.png?ref=main)


Can modify the for loop to start from row 2

So 2 will be the start point and the max row will be the end point

![Image 12](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image12.png?ref=main)

Also including the last row

![Image 13](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image13.png?ref=main)

Now to extract the Supplier name from the row


![Image 14](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image14.png?ref=main)

And can do it using two values, using the row and column number

So in this case the column number is static and the row number is dynamic as that is what we are iterating for

So for the for loop to go through the sheet on column 4 and iterate though the rows, can configure the code to be like the below and append it to a variable, so in essence it will tell us which supplier is in each row


![Image 15](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image15.png?ref=main)

Now we can calculate the amount of product in each row for each supplier

Can include the below code to start that count on a new supplier

![Image 16](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image16.png?ref=main)

However what if it encounters another supplier

We can a check in a loop to see if the supplier name is already appended, if no it will append it, if no it will add another to the count

Like the below

![Image 17](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image17.png?ref=main)

Can then add the iteration to check if the supplier name is there if not can then add it

![Image 18](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image18.png?ref=main)

Can also append the value so that we don’t get cell data and get the actual value, now run the code



And see it a success

![Image 19](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image19.png?ref=main)


#### List each company with respective total inventory value

Next we will calculate the total inventory per supplier

First we can define the dictionary for total value

![Image 20](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image20.png?ref=main)

Then in the loop define the inventory and price variable

![Image 21](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image21.png?ref=main)

And then do the calculations

![Image 22](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image22.png?ref=main)

And then to iterate the calculations for new and old suppliers for the continuous addition

Can do the below logic

![Image 23](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image23.png?ref=main)

Can then run the results and see it has returned the total amount along with the products per supplier

![Image 24](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image24.png?ref=main)


#### List products with inventory less than 10

Next part, we will print out all the products that have inventory less than 10

Can start off by adding a dictionary

![Image 25](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image25.png?ref=main)

And then adding search for product number column

![Image 26](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image26.png?ref=main)

Can then add the logic to check for inventory less than 10

![Image 27](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image27.png?ref=main)

And adding int and re-running them

![Image 28](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image28.png?ref=main)

Can see the product 25, 30 and 74 have products less than 10

![Image 29](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image29.png?ref=main)

#### Calculate and write inventory value for each product into a spreadsheet, thus creating a new column

Next part we will create a new column in the spreadsheet which will calculate the total price of inventory

Can add a new variable in the for loop called inventory price

![Image 30](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image30.png?ref=main)

Can then use ‘value’ to set and update with inventory price which was done earlier

![Image 31](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image31.png?ref=main)

Next we want save the file

Can then call a save function on it

![Image 32](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image32.png?ref=main)

Can also use it for a new name

![Image 33](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image33.png?ref=main)

Lets now print out everything

This is going to be execution of all the logic

![Image 34](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image34.png?ref=main)

And can see the code gets executed successfully

![Image 35](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image35.png?ref=main)

Can see the below new file with the new column of inventory price

![Image 36](https://gitlab.com/FM1995/automation-with-python-spreadsheet/-/raw/main/Images/Image36.png?ref=main)
















